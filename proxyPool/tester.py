#!/usr/bin/env python3 
# -*- coding=utf-8 -*-

import asyncio 
import aiohttp 
import time 
import sys 
try:
	from aiohttp import ClientError 
except:
	from aiohttp import ClientProxyConnectionError as ProxyConnectionError 
from db import RedisClient 
from settings import * 


class Tester(object):
	def __init__(self):
		self.redis = RedisClient()

	# æµè¯åä¸ªä»£ç
	async def test_single_proxy(self, proxy):
		conn = aiohttp.TCPConnector(verify_ssl=False)
		async with aiohttp.ClientSession(connector=conn) as session:
			try:
				if isinstance(proxy, bytes):
					proxy = proxy.decode('utf-8')
				real_proxy = 'http://' + proxy 
				#print('æ­£å¨æµè¯', proxy)
				async with session.get(TEST_URL, proxy=real_proxy, timeout=15, allow_redirects=False) as response:
					if response.status in VALID_STATUS_CODES:
						self.redis.max(proxy)
						# print('ä»£çå¯ç¨', proxy)
					else:
						self.redis.decrease(proxy)
						#print('è¯·æ±ååºç ä¸åæ³', response.status, 'IP', proxy)
			except (ClientError, aiohttp.client_exceptions.ClientConnectorError, asyncio.TimeoutError, AttributeError):
				self.redis.decrease(proxy)
				# print('ä»£çè¯·æ±å¤±è´¥', proxy)

	# æµè¯ä¸»å½æ°
	def run(self):
		print('æµè¯å¨å¼å§è¿è¡')
		try:
			count = self.redis.count()
			print('å½åå©ä½', count, 'ä¸ªä»£ç')
			for i in range(0, count, BATCH_TEST_SIZE):
				start = i 
				stop = min(i + BATCH_TEST_SIZE, count)
				print('æ­£å¨æµè¯ç¬¬', start + 1, '-', stop, 'ä¸ªä»£ç')
				test_proxies = self.redis.batch(start, stop)
				loop = asyncio.get_event_loop()
				tasks = [self.test_single_proxy(proxy) for proxy in test_proxies]
				loop.run_until_complete(asyncio.wait(tasks))
				sys.stdout.flush()
				time.sleep(5)
		except Exception as e:
			print('æµè¯å¨åçéè¯¯', e.args)

