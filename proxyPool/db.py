#!/usr/bin/env python3 
# -*- coding=utf-8 -*-

import redis 
import re 
from random import choice 
from settings import REDIS_HOST, REDIS_PASSWORD, REDIS_PORT, REDIS_KEY
from settings import MAX_SCORE, MIN_SCORE, INITIAL_SCORE


class RedisClient(object):
	# åå§åredis
	def __init__(self, host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PASSWORD):
		self.db = redis.StrictRedis(host=host, port=port, password=password, decode_responses=True)
	
	# æ·»å ä»£çï¼è®¾ç½®åæ°ä¸ºæé«
	def add(self, proxy, score=INITIAL_SCORE):
		if not re.match('\d+.\d+.\d+.\d+\:\d+', proxy):
			print('ä»£çä¸åè§è', proxy, 'ä¸¢å¼')	
		if not self.db.zscore(REDIS_KEY, proxy):
			return self.db.zadd(REDIS_KEY, score, proxy)

	# éæºè·åææä»£çï¼é¦åå°è¯æé«åï¼è¥ä¸å­å¨ï¼åæç§æåï¼å¦åå¼å¸¸
	def random(self):
		result = self.db.zrangebyscore(REDIS_KEY, MAX_SCORE, MAX_SCORE)
		if len(result):
			return choice(result)	
		else:
			result = self.db.zrevrange(REDIS_KEY, 0, 10)
			if len(result):
				return choice(result) 
			else:
				raise PoolEmptyError	

	# ä»£çå¼åä¸åï¼å°äº0åå é¤
	def decrease(self, proxy):
		score = self.db.zscore(REDIS_KEY, proxy)
		if score and score > MIN_SCORE:
			#print('ä»£çè¯·æ±å¤±è´¥', proxy, 'å½ååæ°', score, 'å1')
			return self.db.zincrby(REDIS_KEY, proxy, -1)
		else:
			#print('ä»£çè¯·æ±å¤±è´¥', proxy, 'å½ååæ°', score, 'ç§»é¤')
			return self.db.zrem(REDIS_KEY, proxy)

	# å¤æ­æ¯å¦å­å¨
	def exists(self, proxy):
		return not self.db.zscore(REDIS_KEY, proxy) == None 

	# å°ä»£çè®¾ç½®ä¸ºmax_score
	def max(self, proxy):
		print('ä»£ç', proxy, 'å¯ç¨ï¼è®¾ç½®ä¸º', MAX_SCORE)
		return self.db.zadd(REDIS_KEY, MAX_SCORE, proxy)

	# è·åæ°é
	def count(self):
		return self.db.zcard(REDIS_KEY)

	# è·åå¨é¨ä»£ç
	def all(self):
		return self.db.zrangebyscore(REDIS_KEY, MIN_SCORE, MAX_SCORE)

	def batch(self, start, stop):
		return self.db.zrevrange(REDIS_KEY, start, stop-1)


if __name__ == '__main__':
	conn = RedisClient()
	result = conn.batch(680, 688)
	print(result)	







