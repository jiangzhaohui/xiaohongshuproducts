#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import time 
from multiprocessing import Process 
from api import app 
from getter import Getter 
from tester import Tester 
from db import RedisClient 
from settings import *


class Scheduler(object):
	def schedule_tester(self, cycle=TESTER_CYCLE):
		tester = Tester()
		while True:
			# print('æµè¯å¨å¼å§è¿è¡')
			tester.run()
			time.sleep(cycle)

	def schedule_getter(self, cycle=GETTER_CYCLE):
		getter = Getter()
		while True:
			print('å¼å§æåä»£ç')
			getter.run() 
			time.sleep(cycle)

	def schedule_api(self):
		app.run(API_HOST, API_PORT)

	def run(self):
		# print('ä»£çæ± å¼å§è¿è¡')
		if TESTER_ENABLED:
			tester_process = Process(target=self.schedule_tester)
			tester_process.start()

		if GETTER_ENABLED:
			getter_process = Process(target=self.schedule_getter)
			getter_process.start()

		if API_ENABLED:
			api_process = Process(target=self.schedule_api)
			api_process.start()


if __name__ == '__main__':
	s = Scheduler()
	s.run()