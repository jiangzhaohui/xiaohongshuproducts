# 商品接口： https://www.xiaohongshu.com/api/store/ps/products/v2?keyword=%E6%89%8B%E6%9C%BA&filters=&page=20&size=20&sort=&source=store_feed
import  requests
import json
import time 
import random
import re 
import ast 
from ast import literal_eval
from lxml import etree 
from fake_useragent import UserAgent
from multiprocessing.dummy import Pool as ThreadPool
import gevent 
from gevent import monkey 
gevent.monkey.patch_all()
from gevent.pool import Pool
# from db import *
from configs import db


ua = UserAgent()
headers = {'User-Agent': ua.random}
redis = db.ReidsClient()
url = 'https://www.xiaohongshu.com/api/store/ps/products/v2?keyword={}&filters=&page={}&size=1000&sort=&source=store_feed'


def start_requests(keyword, i=1):
	try:
		rsp = requests.get(url.format(keyword, str(1)), headers=headers, proxies=redis.random(), timeout=15)
	except:
		time.sleep(3)
		start_requests(keyword)
	else:
		if i == 1:
			print('第%s次请求 "%s" 类别' % (i, keyword))
		print('状态码', rsp)
		if rsp.status_code == 200 :
			text = json.loads(rsp.text)
			if len(rsp.text) < 250:
				pass
			else:
				if text.get('data').get('items'):
					if len(text.get('data').get('items')):
						items = text.get('data').get('items')	
						sellers = []
						if items is not None:
							for item in items:
								if {item.get('vendor_name'):item.get('seller_id')} not in sellers:
									sellers.append({item.get('vendor_name'):item.get('seller_id')})
					if sellers is not None:
						for seller in sellers:
							(seller_name, seller_id), = seller.items()
							db.save_stores_data({'seller_name':seller_name, 'seller_id': seller_id})
		else:
			i += 1
			if i < 4:
				print('第%s次请求 %s 类别' % (i, keyword))
				start_requests(keyword, i=i)
			else:
				with open('store_not_found.txt', 'a', encoding='utf-8') as f:
					f.write('请求失败' + keyword + '\n')


if __name__ == '__main__':
	with open('categories.txt', 'r', encoding='utf-8') as f:
		raw_list = f.read()
	products_list = raw_list.split(',')

	pool = Pool(100)
	pool.map(start_requests, [keyword for keyword in products_list[:]])
	pool.kill()
	pool.join()




