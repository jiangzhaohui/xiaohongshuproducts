import pymongo
import redis 
import random 

# redis配置
REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379
REDIS_PASSWORD = None 
REDIS_KEY = 'proxies'

# mongo配置
client = pymongo.MongoClient(host='localhost')
dbm = client.bihu_project
clientb = pymongo.MongoClient('mongodb://testhzbihurw:hzbihutest456@192.168.10.222:27017/testscrapyBiHu')
dbb = clientb.testscrapyBiHu


class ReidsClient(object):
	def __init__(self):
		self.db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PASSWORD)
	def random(self):
		result = self.db.zrangebyscore(REDIS_KEY, 10, 10)
		if len(result):
			proxy = random.choice(result)
			if isinstance(proxy, bytes):
				proxy = proxy.decode('utf-8')
				proxies = {
					'http': 'http://' + proxy,
					'https': 'https://' + proxy,
				}
				# print('正在使用代理：', proxies['http'])
				return proxies


def save_stores_data(data):
	if dbm.xiaohongshu_stores_id_new.update(data, data, upsert=True):
		print('Save successfully...')


def save_products_data(seller_id, seller_name, data):
	if dbm.xiaohongshu_products_new.update(
		{'seller_id': seller_id, 'seller_name': seller_name}, 
		{'$addToSet':
			{
				'products':{'$each': data}
			}
		}, True):
		print('Save successfully...')

# dbm.products.update(
#     {'id': '6'},
#     {'$addToSet':
#         {
#             'follows': {'$each': ['xixi']}
#         }
#     }, True)


def read_stores_data():
	data = []
	for item in dbm.xiaohongshu_stores_id_new.find()[:]:
		data.append(item)
	return data


def convert_to_bihu():
	data = 0
	for item in dbm.xiaohongshu_products.find()[:]:
		data += len(item.get('products'))
	print(data)

