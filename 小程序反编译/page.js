function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function hasMorePage(e) {
    return Boolean(e.hasMore);
}

function createNextPageArgs(e) {
    return e.hasMore || console.log("[page] call create next page with hasMore=false"), 
    {
        page: e.page + 1,
        pageSize: e.pageSize,
        hasMore: e.hasMore
    };
}

function mapPage(e) {
    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [], o = arguments[2], i = o.page, r = o.pageSize, n = void 0, a = void 0;
    return t.length === r ? n = !0 : t.length < r ? n = !1 : (console.log("[page] pageSize is not matched, except: " + r + ", now " + t.length), 
    n = !0), 1 === i ? a = t : i > 1 && (a = e.concat(t)), {
        list: a,
        pagination: {
            page: i,
            pageSize: r,
            hasMore: n
        }
    };
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.DEFAULT_PAGINATION_ARGS = exports.DEFAULT_PAGINATION = exports.getNavigationBarInfo = exports.getScreenRatio = exports.getTrueFullScreenHeight = exports.getTrueScreenHeight = exports.getTopSectionHeight = exports.getTrueStatusBarHeight = exports.getNavigationBarHeight = exports.getNavigationTabBarHeight = exports.androidDeviationHeight = exports.isShowBackIcon = exports.isShowHomePage = exports.widthHeightLikeWhichPhoneModel = exports.getSystemInfo = exports.IPHONE_X = exports.IPHONE_8P = exports.IPHONE_8 = exports.IPHONE_5 = void 0, 
exports.hasMorePage = hasMorePage, exports.createNextPageArgs = createNextPageArgs, 
exports.mapPage = mapPage;

var _path = require("./path.js"), _string = require("./string.js"), _routes = require("./../routes.js"), _api = require("./api.js"), _api2 = _interopRequireDefault(_api), IPHONE_5 = exports.IPHONE_5 = "iPhone 5", IPHONE_8 = exports.IPHONE_8 = "iPhone 8", IPHONE_8P = exports.IPHONE_8P = "iPhone 8P", IPHONE_X = exports.IPHONE_X = "iPhone X", getSystemInfo = exports.getSystemInfo = function() {
    return _api2.default.getSystemInfoSync();
}, noCareLiveSystemInfo = getSystemInfo(), isAndroid = function() {
    return "android" === noCareLiveSystemInfo.platform;
}, widthHeightLikeWhichPhoneModel = exports.widthHeightLikeWhichPhoneModel = function() {
    var e = noCareLiveSystemInfo;
    return e.windowWidth <= 320 ? IPHONE_5 : 375 === e.windowWidth && 667 === e.widthHeight ? IPHONE_8 : e.windowWidth >= 375 && e.windowWidth <= 414 ? IPHONE_8P : 375 === e.windowWidth && e.windowHeight >= 736 ? IPHONE_X : void 0;
}, isShowHomePage = exports.isShowHomePage = function(e) {
    var t = [ _routes.RouteMap.HomePage, _routes.RouteMap.StoreHomePage, _routes.RouteMap.UserCenterPage, _routes.RouteMap.LoginIndex, _routes.RouteMap.GameCompetition, _routes.RouteMap.GameCompetitionChoice, _routes.RouteMap.GameCompetitionLoading, _routes.RouteMap.GameCompetitionResult ], o = !0;
    return t.some(function(t) {
        if (t === e) return o = !1, !0;
    }), o;
}, isShowBackIcon = exports.isShowBackIcon = function(e) {
    var t = [ _routes.RouteMap.GameCompetitionChoice, _routes.RouteMap.GameCompetitionLoading ], o = !0;
    return t.some(function(t) {
        if (t === e) return o = !1, !0;
    }), o;
}, androidDeviationHeight = exports.androidDeviationHeight = 3, getNavigationTabBarHeight = exports.getNavigationTabBarHeight = function() {
    var e = getSystemInfo();
    return e.screenHeight - e.windowHeight;
}, getNavigationBarHeight = exports.getNavigationBarHeight = function() {
    return isAndroid() ? 50 : widthHeightLikeWhichPhoneModel() === IPHONE_5 ? 37 : 44;
}, getTrueStatusBarHeight = exports.getTrueStatusBarHeight = function() {
    var e = getSystemInfo(), t = e.statusBarHeight;
    return isAndroid() && (t += 3), t;
}, getTopSectionHeight = exports.getTopSectionHeight = function() {
    var e = getSystemInfo(), t = getNavigationBarHeight(), o = e.statusBarHeight + t;
    return isAndroid() && (o -= 5), o;
}, getTrueScreenHeight = exports.getTrueScreenHeight = function() {
    var e = getSystemInfo(), t = e.screenHeight - getTopSectionHeight();
    return isAndroid() && e.screenHeight <= e.windowHeight && (t = e.windowHeight - getTopSectionHeight()), 
    t;
}, getTrueFullScreenHeight = exports.getTrueFullScreenHeight = function() {
    var e = getSystemInfo(), t = getTrueScreenHeight();
    return isAndroid() && e.screenHeight !== e.windowHeight && (t += e.statusBarHeight), 
    t;
}, getScreenRatio = exports.getScreenRatio = function() {
    var e = getSystemInfo(), t = e.screenWidth;
    return e.screenHeight / t >= 1.9;
}, getNavigationBarInfo = exports.getNavigationBarInfo = function() {
    var e = {
        canCustom: !0,
        noShow: !1
    }, t = _api2.default.getSystemInfoSync();
    -1 === (0, _string.compareVersion)(t.version, "6.6.0") && (e.canCustom = !1);
    var o = (0, _path.getPageUrl)();
    return _routes.NoNavigationBarRoutes.some(function(t) {
        if (o.route === t) return e.noShow = !0, !0;
    }), e;
}, DEFAULT_PAGINATION = exports.DEFAULT_PAGINATION = {
    PAGE: 1,
    PAGE_SIZE: 10,
    HAS_MORE: !0
}, DEFAULT_PAGINATION_ARGS = exports.DEFAULT_PAGINATION_ARGS = {
    page: DEFAULT_PAGINATION.PAGE,
    pageSize: DEFAULT_PAGINATION.PAGE_SIZE,
    hasMore: DEFAULT_PAGINATION.HAS_MORE
};