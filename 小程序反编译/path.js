function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function getPageUrl() {
    var e = [], t = _api2.default.getGlobalData(), r = t.launchOps || {}, a = getCurrentPages(), o = a[a.length - 1];
    o = o || {};
    var n = o, i = n.route, u = n.options, s = r.scene, p = "";
    i = i || "", u = u || {}, s && (u.scene = s);
    for (var c in u) Object.prototype.hasOwnProperty.call(u, c) && ("from" === c && (p = u[c]), 
    e.push(c + "=" + u[c]));
    return {
        from: p,
        scene: s,
        query: u,
        route: i,
        url: i + (e.length > 0 ? "?" + e.join("&") : "")
    };
}

function isPage(e) {
    return (0, _routes.getRoute)(e);
}

function makePathByRouteKey(e, t) {
    var r = (0, _routes.getRoute)(e);
    if (!r) throw new Error("[path] could not found path via name " + e);
    return t && (r += "?", Object.keys(t).forEach(function(e, a) {
        if (t[e]) {
            a > 0 && (r += "&");
            var o = String(t[e]);
            r += e + "=" + o;
        }
    })), r;
}

function makePath(e, t) {
    var r = (0, _routes.getRoute)(e), a = "";
    if (t && t.plain) return e;
    if (!e && !r) throw new Error("[path] could not found path via name " + e);
    return a = r || e, t && (a += "?", Object.keys(t).forEach(function(e, r) {
        if (t[e]) {
            r > 0 && (a += "&");
            var o = String(t[e]);
            a += e + "=" + o;
        }
    })), a;
}

function makeSharePath(e, t) {
    var r = t || {};
    return r.share = !0, makePath(e, r);
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.makePathByRouteKey = exports.getPageUrl = exports.makeSharePath = exports.makePath = exports.navigateBack = exports.isPage = exports.MINIPROGRAM_STORE = exports.reLaunch = exports.switchTab = exports.redirectTo = exports.navigateTo = void 0;

var _routes = require("./../routes.js"), _api = require("./api.js"), _api2 = _interopRequireDefault(_api), MINIPROGRAM_STORE = {
    TARGET: "miniProgram",
    APP_ID: "wx4554c1e6dfadc8ed",
    VERSION: "release"
}, wrap = function(e) {
    return function(t, r) {
        var a = {}, o = (0, _routes.getRoute)(t);
        return o && (a.url = "/" + o), a.url ? (a.url.indexOf("/webview") > -1 && r.link && "string" == typeof r.link && r.link.indexOf("http") > -1 && (r.link = encodeURIComponent(decodeURIComponent(r.link))), 
        a.url = makePath(a.url, r), _api2.default[e](a)) : void console.error("[path] must provide url parameter");
    };
}, navigateTo = exports.navigateTo = wrap("navigateTo"), redirectTo = exports.redirectTo = wrap("redirectTo"), switchTab = exports.switchTab = wrap("switchTab"), reLaunch = exports.reLaunch = wrap("reLaunch"), navigateBack = function() {
    return _api2.default.navigateBack();
};

exports.MINIPROGRAM_STORE = MINIPROGRAM_STORE, exports.isPage = isPage, exports.navigateBack = navigateBack, 
exports.makePath = makePath, exports.makeSharePath = makeSharePath, exports.getPageUrl = getPageUrl, 
exports.makePathByRouteKey = makePathByRouteKey;