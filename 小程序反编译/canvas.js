function _interopRequireDefault(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

function _asyncToGenerator(t) {
    return function() {
        var e = t.apply(this, arguments);
        return new Promise(function(t, n) {
            function a(r, i) {
                try {
                    var o = e[r](i), s = o.value;
                } catch (t) {
                    return void n(t);
                }
                if (!o.done) return Promise.resolve(s).then(function(t) {
                    a("next", t);
                }, function(t) {
                    a("throw", t);
                });
                t(s);
            }
            return a("next");
        });
    };
}

function _classCallCheck(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var _createClass = function() {
    function t(t, e) {
        for (var n = 0; n < e.length; n++) {
            var a = e[n];
            a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), 
            Object.defineProperty(t, a.key, a);
        }
    }
    return function(e, n, a) {
        return n && t(e.prototype, n), a && t(e, a), e;
    };
}(), _api = require("./api.js"), _api2 = _interopRequireDefault(_api), _canvasImage = require("./canvasImage.js"), _canvasImage2 = _interopRequireDefault(_canvasImage), Canvas = function() {
    function t() {
        _classCallCheck(this, t), this.context = {};
    }
    return _createClass(t, [ {
        key: "initCanvas",
        value: function() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
            this.context = _api2.default.createCanvasContext(t);
        }
    }, {
        key: "drawImage",
        value: function() {
            function t() {
                return e.apply(this, arguments);
            }
            var e = _asyncToGenerator(regeneratorRuntime.mark(function t() {
                var e, n, a, r, i, o = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return regeneratorRuntime.wrap(function(t) {
                    for (;;) switch (t.prev = t.next) {
                      case 0:
                        return e = o.x, n = o.y, a = o.width, r = o.height, t.next = 3, _canvasImage2.default.getImageInfo(o);

                      case 3:
                        i = t.sent, i.path && this.context.drawImage(i.path, e, n, a, r);

                      case 5:
                      case "end":
                        return t.stop();
                    }
                }, t, this);
            }));
            return t;
        }()
    }, {
        key: "drawAvatar",
        value: function() {
            function t() {
                return e.apply(this, arguments);
            }
            var e = _asyncToGenerator(regeneratorRuntime.mark(function t() {
                var e, n, a, r, i, o, s, c, u, l, h, v = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return regeneratorRuntime.wrap(function(t) {
                    for (;;) switch (t.prev = t.next) {
                      case 0:
                        return e = v.x, n = v.y, a = v.width, r = v.borderWidth, i = v.borderColor, o = v.defaultUrl, 
                        s = v.path, c = s, t.next = 4, _canvasImage2.default.privateGetImageInfo(c, 5e3);

                      case 4:
                        if (u = t.sent, l = "", h = "", !o) {
                            t.next = 11;
                            break;
                        }
                        return t.next = 10, _canvasImage2.default.privateGetImageInfo(o);

                      case 10:
                        l = t.sent;

                      case 11:
                        h = u.path ? u.path : l.path, this.context.save(), this.context.beginPath(), this.context.arc(e, n, a / 2, 0, 2 * Math.PI), 
                        this.context.clip(), this.context.drawImage(h, e - a / 2, n - a / 2, a, a), this.context.restore(), 
                        this.context.beginPath(), this.context.arc(e, n, a / 2 + r / 2, 0, 2 * Math.PI), 
                        this.context.setStrokeStyle(i), this.context.setLineWidth(r), this.context.stroke();

                      case 23:
                      case "end":
                        return t.stop();
                    }
                }, t, this);
            }));
            return t;
        }()
    }, {
        key: "drawText",
        value: function() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, e = t.size, n = void 0 === e ? 15 : e, a = t.color, r = void 0 === a ? "black" : a, i = t.text, o = void 0 === i ? "" : i, s = t.x, c = void 0 === s ? 0 : s, u = t.y, l = void 0 === u ? 0 : u, h = t.align, v = void 0 === h ? "left" : h, f = t.isBlod;
            this.context.font = f ? "normal bold " + n + "px sans-serif" : n + "px sans-serif", 
            this.context.setTextAlign(v), this.context.setFontSize(n), this.context.setFillStyle(r), 
            this.context.fillText(o, c, l);
        }
    }, {
        key: "drawRect",
        value: function() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, e = t.color, n = void 0 === e ? "black" : e, a = t.x, r = void 0 === a ? 0 : a, i = t.y, o = void 0 === i ? 0 : i, s = t.width, c = void 0 === s ? 0 : s, u = t.height, l = void 0 === u ? 0 : u;
            this.context.setFillStyle(n), this.context.fillRect(r, o, c, l);
        }
    }, {
        key: "drawTrue",
        value: function() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, e = t.onDrawedCallBack;
            this.context.draw(!0, function() {
                e && e();
            });
        }
    }, {
        key: "getTextWidth",
        value: function() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, e = t.size, n = void 0 === e ? 15 : e, a = t.text, r = void 0 === a ? "" : a, i = t.isBlod;
            return this.context.font = i ? "normal bold " + n + "px sans-serif" : n + "px sans-serif", 
            this.context.measureText(r).width;
        }
    } ]), t;
}();

exports.default = new Canvas();