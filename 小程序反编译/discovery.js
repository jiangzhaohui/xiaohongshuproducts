function _defineProperty(e, r, n) {
    return r in e ? Object.defineProperty(e, r, {
        value: n,
        enumerable: !0,
        configurable: !0,
        writable: !0
    }) : e[r] = n, e;
}

function getFormatedUserTag() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", r = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [], n = e.trim().split("@"), t = [], i = !1;
    return n.forEach(function(e, n) {
        var o = n > 0 && e ? "@" : "", a = !1;
        r.some(function(r) {
            if (e.indexOf(r.nickname) > -1) {
                a = !0, i = !0, t.push({
                    type: "userTag",
                    text: "@ " + r.nickname,
                    id: r.userid || r.id
                });
                var n = e.replace(r.nickname, "");
                return n.trim() && t.push({
                    type: "text",
                    text: n
                }), !0;
            }
        }), !a && e && t.push({
            type: "text",
            text: "" + o + e
        });
    }), {
        result: t,
        hasUserTag: i
    };
}

function getFormatTag() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", r = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [], n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : [], t = JSON.parse(JSON.stringify(r)), i = e.split(/[#]/), o = [];
    return i.forEach(function(e) {
        if (e) {
            var r = !1, i = !1, a = !1;
            if (t.some(function(n) {
                if (n.link = n.link || "", e === n.name && isDeepLink(n) && (a = !0), e === n.name && !isDeepLink(n)) return r = !0, 
                o.push({
                    type: "pageTag",
                    sourceType: n.type,
                    text: e,
                    link: n.link,
                    iconUrl: (0, _icons.getPageTagIconUrl)(n.type)
                }), !0;
            }), a && e && e.trim() && (e = "#" + e + "#"), e.indexOf("@") > -1 && n.length > 0) {
                var s = getFormatedUserTag(e, n);
                i = s.hasUserTag, i && (o = o.concat(s.result));
            }
            r || i || o.push({
                type: "text",
                text: e
            });
        }
    }), o;
}

function getExpressionIcon(e) {
    if (!e) return null;
    var r = JSON.parse(JSON.stringify(ICON_MAPPING_REVERSE));
    for (var n in r) if (n === e) return "https://ci.xiaohongshu.com/xy_emo_" + ICON_MAPPING_REVERSE[n] + ".png?v=2";
    return null;
}

function getFormatedExpressionArr() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", r = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [], n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : [], t = [], i = e, o = JSON.parse(JSON.stringify(r)), a = i.indexOf("[") > -1 && i.indexOf("]") > -1;
    if (!a && i.indexOf("@") > -1 && (a = !0), !a) return [ {
        type: "text",
        text: i
    } ];
    for (i = i.replace(/\[åå]|\[è¯é¢]|\[å°åº]|\[èªå®ä¹]|\[åç]|\[å°ç¹]|\[åºéº]|\[å½±è§]/g, ""); i.length > 0; ) {
        var s = i.indexOf("["), u = i.indexOf("]");
        if (-1 === s || -1 === u || u < s) {
            t = t.concat(getFormatTag(i, o, n)), i = "";
            break;
        }
        t = t.concat(getFormatTag(i.substr(0, s), o, n));
        var _ = i.substr(s + 1, u - s - 1);
        if (getExpressionIcon(_)) t.push({
            type: "image",
            url: getExpressionIcon(_),
            text: i.substr(s, u - s + 1)
        }); else {
            var g = i.substr(s, u - s + 1);
            t = t.concat(getFormatTag(g, o, n));
        }
        i = i.substr(u + 1, i.length);
    }
    return t;
}

function filterUnSupportDiscovery() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [], r = [];
    return e.forEach(function(e) {
        isSupportDiscovery(e) && r.push(e);
    }), r;
}

function getNoTagNoFaceIconText() {
    return (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "").replace(/\[(.+?)\]/g, "");
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.isSupportDiscovery = exports.getGoodsId = void 0;

var _ICON_MAPPING;

exports.getFormatTag = getFormatTag, exports.getExpressionIcon = getExpressionIcon, 
exports.getFormatedExpressionArr = getFormatedExpressionArr, exports.filterUnSupportDiscovery = filterUnSupportDiscovery, 
exports.getNoTagNoFaceIconText = getNoTagNoFaceIconText;

var _enum = require("./enum.js"), _icons = require("./icons.js"), ICON_MAPPING = (_ICON_MAPPING = {
    oldDeyi: "å¾æ",
    xihuan: "åæ¬¢",
    huoli: "æ´»å",
    shaonv: "å°å¥³å¿",
    buman: "ä¸æ»¡",
    taoyan: "è®¨å",
    wuyu: "æ è¯­L",
    oldShengqi: "çæ°",
    koubi: "æ é¼»",
    zhuangku: "è£é·",
    zhenjing: "éæL",
    shihua: "ç³å",
    haipa: "å®³æ",
    kuku: "å­å­",
    fanu: "åæ",
    fuhei: "è¹é»",
    weiqu: "å§å±",
    keshui: "çç¡",
    baji: "å§å§R",
    bishi: "éè§R",
    deyi: "å¾æR",
    feiwen: "é£å»R",
    fuqiang: "æ¶å¢R",
    haixiu: "å®³ç¾R",
    hanyan: "æ±é¢R",
    jingkong: "ææR",
    kure: "å­æ¹R",
    mengmengda: "èèåR",
    sese: "è²è²R",
    shengqi: "çæ°R",
    tanqi: "å¹æ°R",
    touxiao: "å·ç¬R",
    xiaoku: "ç¬å­R",
    zaijian: "åè§R",
    zan: "èµR",
    zhuakuang: "æçR",
    r_xieyan: "æç¼R",
    r_kelian: "å¯æR",
    r_zhoumei: "ç±çR",
    angry: "æ è¯­",
    han: "æ±",
    blind: "ç",
    cool: "é·",
    cry: "å­",
    cute: "è",
    kiss: "ä¹ä¹å",
    dignose: "æé¼»å­",
    koushui: "å£æ°´",
    baiyan: "ç½ç¼",
    frozen: "å¥½å·",
    shy: "å®³ç¾",
    titter: "å¿å¿",
    xixi: "å»å»",
    haha: "åå",
    oops: "å¥½é·",
    question: "åï¼",
    rish: "åè±ª",
    shock: "éæ",
    shoo: "å",
    dizzy: "æ",
    h_tushetou: "åèå¤´H",
    h_jingxia: "æåH",
    h_chelian: "æ¯è¸H",
    r_anzhongguancha: "æä¸­è§å¯R",
    r_chigua: "åçR",
    r_daxiao: "å¤§ç¬R",
    r_heishuwenhao: "é»è¯é®å·R",
    r_henaicha: "åå¥¶è¶R",
    r_huangjinshu: "é»éè¯R"
}, _defineProperty(_ICON_MAPPING, "r_kelian", "å¯æR"), _defineProperty(_ICON_MAPPING, "r_koubi", "æ é¼»R"), 
_defineProperty(_ICON_MAPPING, "r_maibao", "ä¹°çR"), _defineProperty(_ICON_MAPPING, "r_paidui", "æ´¾å¯¹R"), 
_defineProperty(_ICON_MAPPING, "r_shihua", "ç³åR"), _defineProperty(_ICON_MAPPING, "r_shiwang", "å¤±æR"), 
_defineProperty(_ICON_MAPPING, "r_shuijiao", "ç¡è§R"), _defineProperty(_ICON_MAPPING, "r_wa", "åR"), 
_defineProperty(_ICON_MAPPING, "r_weixiao", "å¾®ç¬R"), _defineProperty(_ICON_MAPPING, "r_wulian", "æè¸R"), 
_defineProperty(_ICON_MAPPING, "r_zipai", "èªæR"), _defineProperty(_ICON_MAPPING, "r_nidongde", "ä½ æçR"), 
_defineProperty(_ICON_MAPPING, "r_sang", "ä¸§R"), _ICON_MAPPING), ICON_MAPPING_REVERSE = Object.keys(ICON_MAPPING).reduce(function(e, r) {
    var n = ICON_MAPPING[r];
    return 0 === r.indexOf("old") && r.split("old").length > 1 && (r = r.split("old")[1].toLowerCase()), 
    e[n] = r, e;
}, {}), isDeepLink = function(e) {
    return e.link && e.link.indexOf("xhsdiscover") > -1;
}, getGoodsId = exports.getGoodsId = function(e) {
    var r = e.indexOf("/goods/"), n = e || "";
    return e.indexOf("http") > -1 && (n = e.substr(r + "/goods/".length, 24)), n;
}, isSupportDiscovery = exports.isSupportDiscovery = function(e) {
    return e.type !== _enum.NOTE_TYPE.MULTI;
};