function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function _classCallCheck(e, n) {
    if (!(e instanceof n)) throw new TypeError("Cannot call a class as a function");
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var _createClass = function() {
    function e(e, n) {
        for (var t = 0; t < n.length; t++) {
            var o = n[t];
            o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), 
            Object.defineProperty(e, o.key, o);
        }
    }
    return function(n, t, o) {
        return t && e(n.prototype, t), o && e(n, o), n;
    };
}(), _api = require("./api.js"), _api2 = _interopRequireDefault(_api), _enum = require("./enum.js"), _user = require("./../services/user.js"), _path = require("./path.js"), User = function() {
    function e() {
        _classCallCheck(this, e), this.platform = this.getPlatform(), this.cacheUserInfo = null;
    }
    return _createClass(e, [ {
        key: "getPlatform",
        value: function() {
            var e = _api2.default.getSystemInfoSync().platform;
            return "devtools" === e ? "ios" : e;
        }
    }, {
        key: "getUserInfo",
        value: function() {
            var e = {};
            return this.cacheUserInfo ? e = this.cacheUserInfo : (e = _api2.default.getStorageSync(_enum.STORAGE_KEY.USER_INFO) || {}, 
            this.cacheUserInfo = e), e;
        }
    }, {
        key: "getUserId",
        value: function() {
            return this.getUserInfo().appUserId || "";
        }
    }, {
        key: "getNickname",
        value: function() {
            return this.getUserInfo().appUserInfo.nickname || "";
        }
    }, {
        key: "getWeixinNickname",
        value: function() {
            var e = this.getUserInfo(), n = e.wxUserInfo;
            return n = n || {}, n.nickname || "";
        }
    }, {
        key: "getWxUserInfo",
        value: function() {
            var e = this.getUserInfo(), n = e.wxUserInfo;
            return n = n || {};
        }
    }, {
        key: "setWxUserInfo",
        value: function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
            if (e.avatar && e.nickname) {
                var n = this.getUserInfo();
                n.wxUserInfo = e, this.setUserInfo(n);
            }
        }
    }, {
        key: "isMale",
        value: function() {
            var e = this.getUserInfo(), n = e.appUserInfo;
            return n = n || {}, 0 === n.gender;
        }
    }, {
        key: "setWxUserInfoGender",
        value: function(e) {
            var n = this.getUserInfo(), t = n.wxUserInfo;
            t = t || {}, t.gender = e, this.setUserInfo(n);
        }
    }, {
        key: "isMaleByWeixin",
        value: function() {
            var e = this.getUserInfo(), n = e.wxUserInfo;
            return n = n || {}, 1 === n.gender;
        }
    }, {
        key: "checkLogin",
        value: function() {
            var e = this.getUserInfo();
            return Boolean(e.sid);
        }
    }, {
        key: "ensureLogin",
        value: function() {
            var e = this;
            return new Promise(function(n) {
                e.checkLogin() ? n() : e.goToLogin();
            });
        }
    }, {
        key: "goToLogin",
        value: function() {
            var e = this.getUserInfo();
            if (e.sid) return e.sid = "", void this.setUserInfo(e).then(function() {
                _api2.default.showToast({
                    title: "ç»å½è¿æ",
                    icon: "loading",
                    success: function() {
                        (0, _path.navigateTo)("LoginIndex");
                    }
                });
            });
            (0, _path.navigateTo)("LoginIndex");
        }
    }, {
        key: "privateGetWeixinCode",
        value: function() {
            return new Promise(function(e) {
                _api2.default.login({
                    timeout: 5e3,
                    success: function() {
                        var n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        e(n.code);
                    },
                    fail: function() {
                        e();
                    }
                });
            });
        }
    }, {
        key: "setUserInfo",
        value: function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, n = this.getUserInfo(), t = Object.assign({}, n, e), o = this;
            return new Promise(function(e) {
                _api2.default.setStorage({
                    key: _enum.STORAGE_KEY.USER_INFO,
                    data: t,
                    success: function() {
                        o.cacheUserInfo = t, e(t);
                    },
                    fail: function() {
                        e(!1);
                    }
                });
            });
        }
    }, {
        key: "loginWithCode",
        value: function() {
            var e = this, n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = n.noAutoGoToLogin;
            return new Promise(function(n, o) {
                e.privateGetWeixinCode().then(function(i) {
                    if (!i) return void o();
                    var r = e.getUserInfo(), s = r.isPhoneLogin, a = r.phone, u = r.zone;
                    s ? (0, _user.telephoneLogin)({
                        phone: a,
                        zone: u,
                        code: i
                    }).then(function() {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        e.setUserInfo({
                            userId: t.userId,
                            authorization: t.authorization,
                            openid: t.openid,
                            sid: t.sessionId,
                            unionid: t.unionid,
                            appUserId: t.appUserId,
                            appUserInfo: t.appUserInfo,
                            wxUserInfo: t.wxUserInfo,
                            isNewWxmpUser: t.isNewWxmpUser
                        }).then(function(e) {
                            n(e);
                        });
                    }).catch(function() {
                        t ? n() : e.goToLogin();
                    }) : (0, _user.loginWithCode)(i, r.token).then(function() {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        console.log("/////loginwithcode", t), e.setUserInfo({
                            userId: t.userId,
                            authorization: t.authorization,
                            openid: t.openid,
                            sid: t.sessionId,
                            unionid: t.unionid,
                            appUserId: t.appUserId,
                            appUserInfo: t.appUserInfo,
                            wxUserInfo: t.wxUserInfo,
                            isNewWxmpUser: t.isNewWxmpUser
                        }).then(function(e) {
                            n(e);
                        });
                    });
                });
            });
        }
    }, {
        key: "loginWithWeixinUserInfo",
        value: function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
            e.platform = this.platform;
            var n = this.getUserInfo().token || "";
            return (0, _user.postUserInfoToServerAndGetSid)(e, n);
        }
    }, {
        key: "loginWidthEncryptedData",
        value: function(e) {
            var n = e.encryptedData, t = e.iv, o = {
                encryptedData: n,
                iv: t,
                platform: this.platform
            }, i = this.getUserInfo().token || "";
            return (0, _user.WXMobileLogin)(o, i);
        }
    }, {
        key: "openSettingModal",
        value: function(e, n) {
            _api2.default.showModal({
                title: "æå¼è®¾ç½®é¡µé¢éæ°ææ",
                confirmText: "å»è®¾ç½®",
                cancelText: "åæ¶",
                success: function(t) {
                    t.confirm ? _api2.default.openSetting({
                        success: function(n) {
                            e(n);
                        }
                    }) : n && n();
                }
            });
        }
    } ]), e;
}();

exports.default = new User();