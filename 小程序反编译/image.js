Object.defineProperty(exports, "__esModule", {
    value: !0
});

var Domain = "xiaohongshu.com", Protocol = "http://", IMAGE_SOURCE_TYPE = exports.IMAGE_SOURCE_TYPE = {
    IMG_SOURCE: "img",
    CI_SOURCE: "ci"
}, replaceProtocol = exports.replaceProtocol = function(r) {
    return r = r || "", r.replace(/https?:\/\//, "");
}, isCiUrl = exports.isCiUrl = function(r) {
    return r = r || "", /ci\.xiaohongshu\.com/.test(r);
}, isImageUrl = exports.isImageUrl = function(r) {
    return r = r || "", /img\.xiaohongshu\.com/.test(r);
}, getImageId = exports.getImageId = function(r) {
    if (!(arguments.length > 1 && void 0 !== arguments[1] && arguments[1])) {
        var e = IMAGE_SOURCE_TYPE.CI_SOURCE + "." + Domain + "/";
        r = replaceProtocol(r), r = r.replace(e, "");
    }
    return r.substr(0, 36);
}, getCiUrlById = exports.getCiUrlById = function(r) {
    return IMAGE_SOURCE_TYPE.CI_SOURCE + "." + Domain + "/" + r;
}, getCiUrl = exports.getCiUrl = function(r) {
    r = r || {};
    var e = r, t = e.width, o = e.height, i = e.url, l = e.quality, a = e.abbrevType, s = e.isMogr2, u = e.scale, n = e.blur, c = IMAGE_SOURCE_TYPE.CI_SOURCE + "." + Domain + "/";
    l = l || 92, i = replaceProtocol(i), i = i.replace(c, "");
    var g = getImageId(i, !0), p = [];
    return s ? (u && p.push("/thumbnail/!" + u + "p"), n && p.push("/blur/" + n.radius + "x" + n.sigma), 
    p.push("/quality/" + l), "" + Protocol + c + g + "?imageMogr2" + p.join("")) : (t && p.push("/w/" + t), 
    o && p.push("/h/" + o), p.push("/q/" + l), a = a || 0, "" + Protocol + c + g + "?imageView2/" + a + "/" + p.join(""));
}, getFormatedUrl = exports.getFormatedUrl = function(r) {
    r = r || {};
    var e = r, t = e.url;
    return isCiUrl(t) && (t = getCiUrl(r)), t;
};