function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function isUriProtocol(e) {
    return URI_PROTOCOL.test(e);
}

function isUriSymbol(e) {
    return URI_SYMBOL.test(e);
}

function transUriSymbolToSafe(e) {
    return e.replace(URI_SYMBOL, "https://");
}

function transUriProtocolToSafe(e) {
    return e.replace(URI_PROTOCOL, "https:");
}

function transUriToSafe(e) {
    return isUriProtocol(e) ? transUriProtocolToSafe(e) : isUriSymbol(e) ? transUriSymbolToSafe(e) : e;
}

function changeQuery() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, r = e.link, t = e.type, o = e.key, n = e.value;
    if (!r) return "";
    if (!t) return r;
    if (-1 === [ "set", "remove" ].indexOf(t)) return r;
    var s = (0, _urlParse2.default)(r), i = n || "", a = [], u = [], l = !1;
    return s.query.indexOf("?") > -1 && (a = s.query.split("?")[1].split("&")), a.forEach(function(e) {
        "remove" === t ? -1 === e.indexOf(o + "=") && u.push(e) : "set" === t && (e.indexOf(o + "=") > -1 ? (l = !0, 
        u.push(o + "=" + i)) : u.push(e));
    }), "set" !== t || l || u.push(o + "=" + i), s.set("query", "?" + u.join("&")), 
    s.toString();
}

function cleanSpecialKeys() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", r = e;
    return r = r.replace("?sid", "?mp_env=2&sid"), r = r.replace(/[&|?]sid=session\.[\d]{19}/g, "");
}

function getAbsoluteLink() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, r = e.link;
    if (!r) return "";
    var t = [ /\/goods\/[\S]{24}/, /\/vendor\/[\S]{24}\/events\/[\S]{24}/ ];
    0 !== r.indexOf("/") && 0 !== r.indexOf("http") && (r = "//" + r);
    var o = (0, _urlParse2.default)(r);
    if (([ "pages.xiaohongshu.com", "www.xiaohongshu.com" ].indexOf(o.hostname) > -1 || !o.hostname) && o.set("protocol", "https:"), 
    !o.host) {
        o.set("host", "www.xiaohongshu.com");
        var n = !1;
        t.some(function(e) {
            if (e.test(o.pathname)) return n = !0, !0;
        }), n && o.set("host", "pages.xiaohongshu.com");
    }
    return o.toString();
}

function setSafeProtocol() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
    if (!e) return e;
    var r = (0, _urlParse2.default)(e);
    return r.set("protocol", "https:"), r.toString();
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.isUriProtocol = isUriProtocol, exports.isUriSymbol = isUriSymbol, exports.transUriSymbolToSafe = transUriSymbolToSafe, 
exports.transUriProtocolToSafe = transUriProtocolToSafe, exports.transUriToSafe = transUriToSafe, 
exports.changeQuery = changeQuery, exports.cleanSpecialKeys = cleanSpecialKeys, 
exports.getAbsoluteLink = getAbsoluteLink, exports.setSafeProtocol = setSafeProtocol;

var _urlParse = require("./../npm/url-parse/index.js"), _urlParse2 = _interopRequireDefault(_urlParse), URI_PROTOCOL = /^https?:/, URI_SYMBOL = /^\/\//;