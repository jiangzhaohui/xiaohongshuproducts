function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function _classCallCheck(e, n) {
    if (!(e instanceof n)) throw new TypeError("Cannot call a class as a function");
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var _createClass = function() {
    function e(e, n) {
        for (var t = 0; t < n.length; t++) {
            var i = n[t];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), 
            Object.defineProperty(e, i.key, i);
        }
    }
    return function(n, t, i) {
        return t && e(n.prototype, t), i && e(n, i), n;
    };
}(), _wepy = require("./../npm/wepy/lib/wepy.js"), _wepy2 = _interopRequireDefault(_wepy), Image = function() {
    function e() {
        _classCallCheck(this, e), this.imageInfoCache = {}, this.cdnOrigin = "https://ci.xiaohongshu.com/";
    }
    return _createClass(e, [ {
        key: "privateGetImageInfo",
        value: function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
            return new Promise(function(t, i) {
                var o = void 0;
                n && (o = setTimeout(function() {
                    i();
                }, 3e3)), _wepy2.default.getImageInfo({
                    src: e
                }).then(function(e) {
                    o && clearTimeout(o), t(e);
                }).catch(function() {
                    o && clearTimeout(o), t({});
                });
            });
        }
    }, {
        key: "getImageInfo",
        value: function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, n = this.imageInfoCache, t = this.cdnOrigin, i = this.privateGetImageInfo;
            return new Promise(function(o) {
                var r = e.pathId, a = e.width, u = e.path;
                if (u) return void i(u).then(function() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    o(e);
                });
                var c = a ? r + "_" + a : "" + r;
                if (n[c] && !n[c].isGetting) return void o(n[c].info);
                if (n[c] && n[c].isGetting) return void n[c].queue.push(function(e) {
                    o(e);
                });
                n[c] || (n[c] = {
                    queue: [],
                    info: {},
                    isGetting: !0
                });
                var f = u || "" + t + r;
                a && !u && (f = f + "?imageView2/0//w/" + a, console.log(f)), i(f).then(function() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = e.path;
                    n[c].isGetting = !1, e.url = f, n[c].info = e, n[c].queue.forEach(function(n) {
                        n(e);
                    }), n[c].queue = [], t || delete n[c], o(e);
                });
            });
        }
    } ]), e;
}();

exports.default = new Image();