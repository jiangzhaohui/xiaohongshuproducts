function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function processResponseData(e) {
    return {
        data: e.data,
        statusCode: e.statusCode
    };
}

function processResponseError(e) {
    var t = e.response;
    return {
        data: t && t.data || e.data,
        statusCode: t && t.status,
        errorMsg: e.message || e.errMsg,
        errorCode: e.code
    };
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.configure = exports.interceptors = exports.makeUri = exports.patch = exports.put = exports.post = exports.head = exports.del = exports.get = void 0;

var _api = require("./api.js"), _api2 = _interopRequireDefault(_api), _http = require("./../libs/@xhs/http.js"), _http2 = _interopRequireDefault(_http), _auth = require("./auth.js"), _deepClone = require("./../libs/deep-clone.js"), _deepClone2 = _interopRequireDefault(_deepClone), _enum = require("./enum.js");

if (!_api2.default.hasMpApi()) throw new Error("[Http Exception] could not found wepy instance");

var _sendRequest = function(e) {
    var t = _api2.default.getStorageSync(_enum.STORAGE_KEY.USER_INFO) || {}, r = t.authorization;
    r && -1 === e.url.indexOf("/api/cs/v2/images/tokens") && (e.header ? e.header.Authorization = r : e.header = {
        Authorization: r
    });
    var o = (0, _deepClone2.default)(e);
    return new Promise(function(t, r) {
        o.success = function() {
            var o = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
            if (200 !== o.statusCode) r(o); else {
                var a = o.data;
                if ("[object String]" === Object.prototype.toString.call(a) && "{" === a[0] && "}" === a[a.length - 1]) {
                    a = a.replace(/\s/g, "");
                    try {
                        o.data = JSON.parse(a);
                    } catch (e) {}
                }
                try {
                    o.data.requestUrl = e.url;
                } catch (e) {
                    console.error(e);
                }
                t(o);
            }
        }, o.fail = function() {
            t({});
        }, _api2.default.request(o);
    });
}, _sendAuthRequest = function(e) {
    var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
    return (0, _auth.requestToken)(t).then(function(t) {
        var r = (t, e);
        return e.header ? (e.header.Auth = t, (0, _auth.encryptToken)e.header["Auth-Sign"] = r) : e.header = {
            Auth: t,
            "Auth-Sign": r
        }, _sendRequest(e);
    }); 
}, authReg = /https?:\/\/www\.xiaohongshu.com\/sapi\/(web_api|wx_mp_api)\/.*/, request = function() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
    if (e.params) {
        var t = Object.keys(e.params).map(function(t) {
            return encodeURI(decodeURI(t)) + "=" + encodeURI(decodeURI(e.params[t]));
        });
        e.url = e.url + "?" + t.join("&"), delete e.params;
    }
    return authReg.test(e.url) ? _sendAuthRequest(e).catch(function(t) {
        if (401 === t.statusCode) return _sendAuthRequest(e, !0);
        throw t;
    }) : (e.url = e.url.replace(/\/sapi\//, "/"), _sendRequest(e));
}, adapts = {
    processResponseData: processResponseData,
    processResponseError: processResponseError
}, _factory = (0, _http2.default)(request, adapts), get = _factory.get, del = _factory.del, head = _factory.head, post = _factory.post, put = _factory.put, patch = _factory.patch, makeUri = _factory.makeUri, interceptors = _factory.interceptors, configure = _factory.configure, wxHttpConfigure = function(e) {
    return configure(e);
};

exports.get = get, exports.del = del, exports.head = head, exports.post = post, 
exports.put = put, exports.patch = patch, exports.makeUri = makeUri, exports.interceptors = interceptors, 
exports.configure = wxHttpConfigure;



// auth: eyJoYXNoIjoibWQ0IiwiYWxnIjoiSFMyNTYiLCJ0eXAiOiJKV1QifQ.eyJzaWQiOiJiMTQxOWMxMWYzODk0Mzg1YzMzYzBkMzk0ODQxYmM1YiIsImV4cGlyZSI6MTU0NjQwMzk0OH0.DsCGMtjUiohwq2URlKgQOGPI0jlSmNwtQzvua0plzIc
// content-type: application/json
// auth-sign: 7e567893a6aa3d23161e064ccbd8bf5e


// auth: eyJoYXNoIjoibWQ0IiwiYWxnIjoiSFMyNTYiLCJ0eXAiOiJKV1QifQ.eyJzaWQiOiJiMTQxOWMxMWYzODk0Mzg1YzMzYzBkMzk0ODQxYmM1YiIsImV4cGlyZSI6MTU0NjQwMzk0OH0.DsCGMtjUiohwq2URlKgQOGPI0jlSmNwtQzvua0plzIc
// content-type: application/json
// auth-sign: 9550a039247d06439d8af6f4bdd8e297

// auth: eyJoYXNoIjoibWQ0IiwiYWxnIjoiSFMyNTYiLCJ0eXAiOiJKV1QifQ.eyJzaWQiOiJiMTQxOWMxMWYzODk0Mzg1YzMzYzBkMzk0ODQxYmM1YiIsImV4cGlyZSI6MTU0NjQwMzk0OH0.DsCGMtjUiohwq2URlKgQOGPI0jlSmNwtQzvua0plzIc
// content-type: application/json
// auth-sign: 0eb33872d4c1fa0b12e8cd7400644e37

// auth: eyJoYXNoIjoibWQ0IiwiYWxnIjoiSFMyNTYiLCJ0eXAiOiJKV1QifQ.eyJzaWQiOiIwMjQwNTE2NjQ2NWQ0YTJiY2ExYjU2NTcwZTBmYTZhNyIsImV4cGlyZSI6MTU0NjQxNjMwOH0.Y90whBmPRlxltqYPBsHg-HQKcP3SHfGxbSyWNbBhEvM
// content-type: application/json
// auth-sign: 0f0340029e465b0372c0bf37cff0a352
