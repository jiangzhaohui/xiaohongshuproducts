function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function getStorage() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
    return _getStorage(e, !1);
}

function getStorageSync() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
    return _getStorage(e, !0);
}

function setStorageSync(e) {
    var t = e.key, r = e.data, a = e.expireHours, n = parseExpire(a);
    return _setStorage(t, r, n, !0);
}

function setStorage(e) {
    var t = e.key, r = e.data, a = e.expireHours, n = parseExpire(a);
    return _setStorage(t, r, n, !1);
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.getStorage = getStorage, exports.getStorageSync = getStorageSync, exports.setStorageSync = setStorageSync, 
exports.setStorage = setStorage;

var _wepy = require("./../npm/wepy/lib/wepy.js"), _wepy2 = _interopRequireDefault(_wepy), _api = require("./api.js"), _api2 = _interopRequireDefault(_api), _enum = require("./enum.js"), isObject = function(e) {
    return "[object Object]" === Object.prototype.toString.call(e);
}, _getStorage = function() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1], r = _enum.STORAGE_KEY[e];
    if (!r) throw new Error("not find this key " + e + " in STORAGE_KEY");
    var a = Date.parse(new Date());
    if (t) {
        var n = _api2.default.getStorageSync(r);
        if (isObject(n)) {
            var o = n.expire, i = n.data;
            return 0 !== o && o < a ? void 0 : i;
        }
        return n;
    }
    return _wepy2.default.getStorage({
        key: r
    }).then(function(e) {
        var t = e.data;
        if (e.data) {
            if (isObject(t)) {
                var r = t.expire, n = t.data;
                return 0 !== r && r < a ? void 0 : n;
            }
            return t;
        }
    });
}, _setStorage = function() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", t = arguments[1], r = arguments[2], a = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3], n = _enum.STORAGE_KEY[e], o = {
        expire: r,
        data: t
    };
    if (!n) throw new Error("not find this key " + e + " in STORAGE_KEY");
    return a ? _wepy2.default.setStorageSync(n, o) : _wepy2.default.setStorage({
        key: n,
        data: o
    });
}, parseExpire = function(e) {
    return isNaN(e) ? 0 : 60 * e * 60 * 1e3 + Date.parse(new Date());
};