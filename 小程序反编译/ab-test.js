function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function _classCallCheck(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var _createClass = function() {
    function e(e, t) {
        for (var i = 0; i < t.length; i++) {
            var a = t[i];
            a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), 
            Object.defineProperty(e, a.key, a);
        }
    }
    return function(t, i, a) {
        return i && e(t.prototype, i), a && e(t, a), t;
    };
}(), _api = require("./api.js"), _api2 = _interopRequireDefault(_api), _enum = require("./enum.js"), _system = require("./../services/system.js"), AbTest = function() {
    function e() {
        _classCallCheck(this, e), this.gotDataFromApi = !1, this.isFetchingApi = !1, this.queues = [], 
        this.cacheDatas = null;
    }
    return _createClass(e, [ {
        key: "clearQueue",
        value: function() {
            this.queues && this.queues.length > 0 && (this.queues.forEach(function(e) {
                e && e();
            }), this.queues = []);
        }
    }, {
        key: "getSid",
        value: function() {
            return _api2.default.getStorageSync(_enum.STORAGE_KEY.USER_INFO).sid;
        }
    }, {
        key: "init",
        value: function(e) {
            var t = this, i = this.getSid();
            this.isFetchingApi || this.gotDataFromApi || !i || (this.isFetchingApi = !0, (0, 
            _system.getAbTest)().then(function(i) {
                "[object Object]" === Object.prototype.toString.call(i) && _api2.default.setStorageSync(_enum.STORAGE_KEY.AB_TEST, {
                    expIds: i.exp_ids || [],
                    flags: i.flags || []
                }), t.isFetchingApi = !1, t.gotDataFromApi = !0, t.clearQueue(), i.success || (t.isFetchingApi = !1, 
                t.gotDataFromApi = !1, t.clearQueue()), e && e();
            }).catch(function() {
                t.isFetchingApi = !1, t.gotDataFromApi = !0, t.clearQueue(), e && e();
            }));
        }
    }, {
        key: "pageABTestMetaReady",
        value: function() {
            var e = this;
            return new Promise(function(t) {
                var i = e.getSid();
                !e.gotDataFromApi && i ? e.queues.push(t) : t();
            });
        }
    }, {
        key: "privateGetCacheDatas",
        value: function() {
            if (this.cacheDatas) return this.cacheDatas;
            var e = _api2.default.getStorageSync(_enum.STORAGE_KEY.AB_TEST), t = e.expIds, i = e.flags;
            return this.cacheDatas = {
                expIds: t || [],
                flags: i || {}
            }, this.cacheDatas;
        }
    }, {
        key: "getExpIds",
        value: function() {
            return this.privateGetCacheDatas().expIds || [];
        }
    }, {
        key: "getABTestFlagValue",
        value: function(e, t) {
            var i = this.privateGetCacheDatas(), a = i.expIds, s = i.flags, u = void 0;
            if (s = s || {}, a = a || [], e && t && "exp" === t && a.some(function(t) {
                if (0 === t.indexOf(e)) return u = t.substr(e.length, 1), !0;
            }), e && !t && Object.keys(s).length > 0) {
                var n = s.fulishe && void 0 !== s.fulishe[e] ? s.fulishe[e] : null, r = s.shequ && void 0 !== s.shequ[e] ? s.shequ[e] : null;
                void 0 !== n && (u = n), void 0 !== r && (u = r);
            }
            return u;
        }
    } ]), e;
}();

exports.default = new AbTest();