function contains(_, E) {
    for (var e in _) if (_.hasOwnProperty(e) && _[e] === E) return !0;
    return !1;
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.contains = contains;

var TRACK = exports.TRACK = {
    className: "xhs-track"
}, TRENDING_TYPE = exports.TRENDING_TYPE = {
    SEARCH_TRENDING: "search_trending",
    GOODS_TRENDING: "goods_trending"
}, NOTE_TYPE = exports.NOTE_TYPE = {
    NORMAL: "normal",
    MULTI: "multi",
    VIDEO: "video"
}, STORAGE_KEY = exports.STORAGE_KEY = {
    API_TOKEN: "api_token",
    LAST_SEARCH: "last_search",
    USER_INFO: "user_info",
    PRODUCT_SEARCH_RECORDS: "product_search_records",
    NEWBIE_FLAG: "newbie_flag",
    PIN_MINI_PROGRAM_FLAG: "pin_mini_program",
    CATEGORY: "category",
    HOME_PAGE_FEEDS: "homePageFeeds",
    WX_USER_INFO_DETAIL: "wx_user_info_detail",
    IS_GET_NEW_FEEDS: "is_get_new_feeds",
    AB_TEST: "xhs_ab_test",
    IS_NEW_WXMP_UESR: "is_new_wxmp_user",
    SELECTED_STAR: "selected_star",
    CAN_SHOW_FULL_ADD_MP: "canShowFullAddMp"
}, SHARE_GROUP_CARD = exports.SHARE_GROUP_CARD = 1008, SHARE_SINGLE_CARD = exports.SHARE_SINGLE_CARD = 1007, SHARE_MEESAGE_SCENE = exports.SHARE_MEESAGE_SCENE = 1036, MAIN_CHAT_SCENE = exports.MAIN_CHAT_SCENE = 1089, HISTORY_SCENE = exports.HISTORY_SCENE = 1090;