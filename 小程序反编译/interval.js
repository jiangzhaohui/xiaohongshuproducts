function _classCallCheck(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var _createClass = function() {
    function e(e, t) {
        for (var a = 0; a < t.length; a++) {
            var n = t[a];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), 
            Object.defineProperty(e, n.key, n);
        }
    }
    return function(t, a, n) {
        return a && e(t.prototype, a), n && e(t, n), t;
    };
}(), _path = require("./path.js"), IntervalUtil = function() {
    function e() {
        _classCallCheck(this, e), this.intervalMaps = {}, this.nowPageKey = null;
    }
    return _createClass(e, [ {
        key: "addIntervalTask",
        value: function(e, t) {
            var a = (0, _path.getPageUrl)(), n = a.url;
            this.nowPageKey = n;
            var r = this.nowPageKey;
            this.intervalMaps[r] || (this.intervalMaps[r] = []);
            var i = setInterval(e, t);
            this.intervalMaps[r].push({
                fn: e,
                time: t,
                task: i
            });
        }
    }, {
        key: "sleep",
        value: function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", t = this.intervalMaps[e];
            t && (t.forEach(function(e) {
                clearTimeout(e.task), e.task = null;
            }), this.intervalMaps[e] = t);
        }
    }, {
        key: "wakeUp",
        value: function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", t = this.intervalMaps[e];
            t && t.forEach(function(e) {
                var t = e.fn, a = e.time;
                e.task = setInterval(t, a);
            });
        }
    }, {
        key: "clean",
        value: function() {
            if (this.intervalMaps && Object.keys(this.intervalMaps).length > 0) for (var e in this.intervalMaps) Object.prototype.hasOwnProperty.call(this.intervalMaps, e) && this.intervalMaps[e].forEach(function(e) {
                clearTimeout(e.task), e.task = null;
            });
            this.intervalMaps = {};
        }
    } ]), e;
}();

exports.default = new IntervalUtil();