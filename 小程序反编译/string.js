function escapeNoteString() {
    return (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "").replace(/(\u2028|\u2029)/g, "\n");
}

function compareVersion() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
    e = e.split("."), t = t.split(".");
    for (var r = Math.max(e.length, t.length); e.length < r; ) e.push("0");
    for (;t.length < r; ) t.push("0");
    for (var n = 0; n < r; n++) {
        var u = parseInt(e[n], 10), o = parseInt(t[n], 10);
        if (u > o) return 1;
        if (u < o) return -1;
    }
    return 0;
}

function subMaxLengthString() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", t = arguments[1], r = e.match(/\ud83c[\udf00-\udfff]|\ud83d[\udc00-\ude4f]|\ud83d[\ude80-\udeff]|\S/g), n = r.length, u = [], o = 0;
    return n > t ? (r.some(function(e) {
        if (o > t - 3) return !0;
        u.push(e), o++;
    }), u.join("") + "...") : e;
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.escapeNoteString = escapeNoteString, exports.compareVersion = compareVersion, 
exports.subMaxLengthString = subMaxLengthString;