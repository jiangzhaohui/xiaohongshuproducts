function _classCallCheck(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var _createClass = function() {
    function e(e, t) {
        for (var n = 0; n < t.length; n++) {
            var r = t[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), 
            Object.defineProperty(e, r.key, r);
        }
    }
    return function(t, n, r) {
        return n && e(t.prototype, n), r && e(t, r), t;
    };
}(), SystemUtil = function() {
    function e() {
        _classCallCheck(this, e), this.systemInfo = wx.getSystemInfoSync();
    }
    return _createClass(e, [ {
        key: "isAndroid",
        value: function() {
            return "android" === this.systemInfo.platform;
        }
    } ]), e;
}();

exports.default = new SystemUtil();