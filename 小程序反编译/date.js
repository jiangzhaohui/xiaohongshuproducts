function _classCallCheck(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
}

function dateStringToTimestamp(e) {
    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : TIMEZONE;
    return new Date(e.trim().replace(" ", "T") + "" + t).getTime();
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
    return typeof e;
} : function(e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
}, _createClass = function() {
    function e(e, t) {
        for (var r = 0; r < t.length; r++) {
            var a = t[r];
            a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), 
            Object.defineProperty(e, a.key, a);
        }
    }
    return function(t, r, a) {
        return r && e(t.prototype, r), a && e(t, a), t;
    };
}();

exports.dateStringToTimestamp = dateStringToTimestamp;

var TIMEZONE = "+08:00", DAY = 864e5, HOUR = 36e5, MINUTE = 6e4, SECOND = 1e3, DATE_TIME_MASKS = {
    default: "YYYY-MM-DD HH:mm:ss",
    shortTime: "mm:ss",
    mediumTime: "HH:mm:ss",
    longTime: "DD HH:mm:ss"
}, TIME_MASKS = {
    shortTime: "mm: ss",
    mediumTimeDistance: "è·ç¦»ç»æè¿æ HH: mm: ss",
    mediumTimeRemain: "è¿å©HHå°æ¶mmåssç§",
    longTime: "è¿å©Då¤©HHå°æ¶mmåssç§"
}, REGTOVALUE = {
    "D+": "days",
    "H+": "hours",
    "m+": "minutes",
    "s+": "seconds"
}, DateTimeFormatter = function() {
    function e(t, r) {
        _classCallCheck(this, e), this.fmt = DATE_TIME_MASKS[t] || t, this.timezone = r, 
        this.week = {
            0: "/u65e5",
            1: "/u4e00",
            2: "/u4e8c",
            3: "/u4e09",
            4: "/u56db",
            5: "/u4e94",
            6: "/u516d"
        };
    }
    return _createClass(e, [ {
        key: "getODate",
        value: function(e) {
            return {
                "M+": {
                    regexp: /(M+)/,
                    value: e.getMonth() + 1
                },
                "D+": {
                    regexp: /(D+)/,
                    value: e.getDate()
                },
                "h+": {
                    regexp: /(h+)/,
                    value: e.getHours() % 12 == 0 ? 12 : e.getHours() % 12
                },
                "H+": {
                    regexp: /(H+)/,
                    value: e.getHours()
                },
                "m+": {
                    regexp: /(m+)/,
                    value: e.getMinutes()
                },
                "s+": {
                    regexp: /(s+)/,
                    value: e.getSeconds()
                },
                "q+": {
                    regexp: /(q+)/,
                    value: Math.floor((e.getMonth() + 3) / 3)
                },
                S: {
                    regexp: /(S)/,
                    value: e.getMilliseconds()
                }
            };
        }
    }, {
        key: "normalize",
        value: function(e) {
            if (isNaN(Number(e)) || "[object Null]" === Object.prototype.toString.call(e)) throw new Error("Wrong parameter: dateSource " + e);
            if (e instanceof Date) return e;
            var t = this.adjustTimezone(e * SECOND, this.timezone);
            return new Date(t);
        }
    }, {
        key: "adjustTimezone",
        value: function(e) {
            var t = Number(this.timezone.replace(":00", "")) * (MINUTE / SECOND);
            return e + (new Date().getTimezoneOffset() + t) * MINUTE;
        }
    }, {
        key: "format",
        value: function(e) {
            var t = this.normalize(e), r = this.getODate(t);
            /(Y+)/.test(this.fmt) && (this.fmt = this.fmt.replace(RegExp.$1, (t.getFullYear() + "").substr(4 - RegExp.$1.length))), 
            /(E+)/.test(this.fmt) && (this.fmt = this.fmt.replace(RegExp.$1, (RegExp.$1.length > 1 ? RegExp.$1.length > 2 ? "/u661f/u671f" : "/u5468" : "") + this.week[t.getDay() + ""]));
            for (var a in r) r[a].regexp.test(this.fmt) && (this.fmt = this.fmt.replace(RegExp.$1, 1 === RegExp.$1.length ? r[a].value : ("00" + r[a].value).substr(("" + r[a].value).length)));
            return this.fmt;
        }
    } ]), e;
}(), TimeFormatter = function() {
    function e(t) {
        _classCallCheck(this, e), this.fmt = TIME_MASKS[t] || t;
    }
    return _createClass(e, [ {
        key: "getOTime",
        value: function(e) {
            var t = this.getTimestampObject(e);
            return {
                "D+": {
                    regexp: /(D+)/,
                    value: t.days
                },
                "H+": {
                    regexp: /(H+)/,
                    value: t.hours
                },
                "m+": {
                    regexp: /(m+)/,
                    value: t.minutes
                },
                "s+": {
                    regexp: /(s+)/,
                    value: t.seconds
                }
            };
        }
    }, {
        key: "getTimestampObject",
        value: function(e) {
            return {
                days: Math.floor(e / DAY),
                hours: Math.floor(e % DAY / HOUR),
                minutes: Math.floor(e % HOUR / MINUTE),
                seconds: Math.floor(e % MINUTE / SECOND)
            };
        }
    }, {
        key: "getAutoFmt",
        value: function() {
            for (var t = e.splitFmt(this.fmt), r = [], a = !0, i = t.length, s = this.getTimestampObject(this.timestamp), n = 0; n < i; ) {
                var o = t[n], u = e.isMatch(o), m = s[e.searchKey(o)];
                if (u || 0 !== n) if (a && u) {
                    if (0 === m && i - n > 2) {
                        e.isMatch(t[n + 1]) ? n++ : n += 2;
                        continue;
                    }
                    n++, a = !1, r.push(m);
                } else u ? (r.push(("00" + s[e.searchKey(o)]).slice(-o.length)), n++) : (r.push(o), 
                n++); else r.push(o), n++;
            }
            return r.join("");
        }
    }, {
        key: "format",
        value: function(t, r, a) {
            if (this.timestamp = e.normalize(t), a) {
                var i = e.splitFmt(this.fmt), s = this.getTimestampObject(this.timestamp), n = [];
                return i.forEach(function(t) {
                    e.isMatch(t) ? n.push({
                        key: t,
                        value: ("00" + s[e.searchKey(t)]).slice(-t.length)
                    }) : n.push({
                        key: "separator",
                        value: t
                    });
                }), n;
            }
            if (r) return this.getAutoFmt();
            var o = this.getOTime(this.timestamp);
            for (var u in o) o[u].regexp.test(this.fmt) && (this.fmt = this.fmt.replace(RegExp.$1, 1 === RegExp.$1.length ? o[u].value : ("00" + o[u].value).substr(("" + o[u].value).length)));
            return this.fmt;
        }
    } ], [ {
        key: "normalize",
        value: function(e) {
            if (isNaN(Number(e)) || "object" === (void 0 === e ? "undefined" : _typeof(e))) throw new Error("Wrong parameter: dateSource " + e);
            return Number(e);
        }
    }, {
        key: "isMatch",
        value: function(e) {
            return /D+|H+|m+|s+/g.test(e);
        }
    }, {
        key: "splitFmt",
        value: function(e) {
            return e.match(/D+|H+|m+|s+|[^D+|H+|m+|s+]+/g);
        }
    }, {
        key: "searchKey",
        value: function(e) {
            var t = "";
            for (var r in REGTOVALUE) if (Object.prototype.hasOwnProperty.call(REGTOVALUE, r)) {
                var a = RegExp("" + r, "g");
                a.test(e) && (t = REGTOVALUE[r]);
            }
            return t;
        }
    } ]), e;
}(), formatDateTime = function(e) {
    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "default", r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : TIMEZONE;
    return new DateTimeFormatter(t, r).format(e);
}, formatTime = function(e) {
    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "shortTime", r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {}, a = r.auto, i = void 0 !== a && a, s = r.fmtArray;
    return new TimeFormatter(t).format(e, i, s);
};

exports.default = {
    formatDateTime: formatDateTime,
    formatTime: formatTime
};