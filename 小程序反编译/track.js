function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function _classCallCheck(e, r) {
    if (!(e instanceof r)) throw new TypeError("Cannot call a class as a function");
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.trackImpression = exports.trackPageview = exports.trackClick = exports.trackError = exports.trackNormalData = void 0;

var _createClass = function() {
    function e(e, r) {
        for (var t = 0; t < r.length; t++) {
            var a = r[t];
            a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), 
            Object.defineProperty(e, a.key, a);
        }
    }
    return function(r, t, a) {
        return t && e(r.prototype, t), a && e(r, a), r;
    };
}(), _routes = require("./../routes.js"), _path = require("./path.js"), _enum = require("./enum.js"), _api = require("./api.js"), _api2 = _interopRequireDefault(_api), _tracker = require("./../services/tracker.js"), SHOW_PART_RATE = .3, getPageUrlInfo = function() {
    var e = (0, _path.getPageUrl)();
    try {
        var r = e.query || {}, t = r.gdt_vid, a = r.weixinadinfo, o = {};
        if (t && a) {
            var n = a.split(".");
            o.aid = n[0];
        }
        return o = Object.assign(o, e);
    } catch (r) {
        return console.warn(r), e;
    }
}, trackNormalData = exports.trackNormalData = function(e) {
    var r = e.action, t = e.label, a = e.property, o = e.context, n = e.sourceRoute, i = getPageUrlInfo();
    if (i && i.route || n) {
        var c = i.route, s = i.url, l = i.from, u = i.aid, f = (0, _routes.getCategory)(c || n);
        o = o || {}, o.scene = i.scene, (0, _tracker.sendTrackInfo)({
            category: f,
            seAc: r,
            sePr: a,
            seLa: t,
            url: s,
            from: l,
            co: o,
            aid: u
        });
    }
}, trackError = exports.trackError = function() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", r = getPageUrlInfo();
    if (r && r.route) {
        var t = r.route, a = r.url, o = r.from, n = r.aid, i = {
            category: (0, _routes.getCategory)(t) || "mpPage",
            seAc: "error",
            sePr: "",
            seLa: e.substr(0, 60),
            url: a,
            from: o,
            aid: n,
            co: {
                errMsg: e.substr(0, 220)
            }
        };
        (0, _tracker.sendTrackInfo)(i);
    }
}, trackClick = exports.trackClick = function() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, r = {}, t = getPageUrlInfo(), a = t.route, o = t.url, n = t.from, i = t.scene, c = t.aid;
    try {
        "[object String]" === Object.prototype.toString.call(e) && (r = JSON.parse(e)), 
        "[object Object]" === Object.prototype.toString.call(e) && (r = e);
    } catch (e) {
        return void console.warn("The id attribute of track element must be json stringify");
    }
    var s = r, l = s.label, u = s.property, f = s.context;
    f = f || {}, f.scene = i;
    var p = {
        category: (0, _routes.getCategory)(a) || "mpPage",
        seAc: l + "_click",
        sePr: u,
        url: o,
        from: n,
        co: f,
        aid: c
    };
    (0, _tracker.sendTrackInfo)(p);
}, trackPageview = exports.trackPageview = function() {
    var e = getPageUrlInfo(), r = e.route, t = e.query, a = e.url, o = e.scene, n = e.from, i = e.aid;
    (0, _tracker.sendTrackInfo)({
        category: (0, _routes.getCategory)(r) || "mpPage",
        seAc: "pv",
        sePr: t.id || "",
        seLa: o,
        co: {
            scene: o
        },
        url: a,
        from: n,
        aid: i
    });
}, impressionScrollMap = {}, trackImpression = exports.trackImpression = function() {
    function e(r) {
        var t = this;
        _classCallCheck(this, e), this.findTrackFailNum = 0, this.impressedTracker = {}, 
        this.pageInstance = r, this.initWindowSize(), this.countDown = null;
        var a = r.$wxpage.route;
        impressionScrollMap[a] || (impressionScrollMap[a] = 1, this.countDown = setTimeout(function() {
            t.postData();
        }, 300), this.trackEvent());
    }
    return _createClass(e, [ {
        key: "trackEvent",
        value: function() {
            var e = this, r = this, t = r.pageInstance, a = t.onPageScroll || null;
            if (t.onPageScroll = function(o) {
                a && a.call(t, o), e.countDown && clearTimeout(e.countDown), e.countDown = setTimeout(function() {
                    Object.keys(r.impressedTracker).length > 100 && (r.impressedTracker = {}), r.postData();
                }, 500);
            }, t.onPullDownRefresh) {
                var o = t.onPullDownRefresh;
                t.onPullDownRefresh = function() {
                    r.clearImpressedTracker(), o.call(t);
                };
            } else t.onPullDownRefresh = function() {
                r.clearImpressedTracker();
            };
        }
    }, {
        key: "initWindowSize",
        value: function() {
            var e = this.pageInstance.$root.$parent.globalData.systemInfo;
            this.windowHeight = e.windowHeight, this.windowWidth = e.windowWidth;
        }
    }, {
        key: "postData",
        value: function() {
            var e = this, r = (0, _path.getPageUrl)(), t = r.route, a = r.url, o = r.from, n = r.scene;
            if (_api2.default.canIUse("createSelectorQuery") && _api2.default.createSelectorQuery) {
                _api2.default.createSelectorQuery().selectAll("." + _enum.TRACK.className).boundingClientRect(function(r) {
                    r.forEach(function(r) {
                        var i = {};
                        try {
                            i = JSON.parse(r.id);
                        } catch (e) {
                            return void console.warn("The id attribute of track element must be json stringify");
                        }
                        var c = i, s = c.label, l = c.property, u = c.context, f = c.timeStamp;
                        s = s || "", l = l || "", u = u || {}, u.scene = n, f = f || 0, e.label = s;
                        var p = "" + s + l + f;
                        if (!e.impressedTracker[p]) {
                            if (e.isInnerWindow(r)) {
                                var g = {
                                    category: (0, _routes.getCategory)(t) || "mpPage",
                                    seAc: s + "_impression",
                                    sePr: l,
                                    url: a,
                                    co: u,
                                    from: o
                                };
                                e.impressedTracker[p] = 1, (0, _tracker.sendTrackInfo)(g);
                            }
                        }
                    });
                }).exec();
            }
        }
    }, {
        key: "clearImpressedTracker",
        value: function() {
            this.impressedTracker = {}, this.findTrackFailNum = 0;
        }
    }, {
        key: "isInnerWindow",
        value: function(e) {
            if (e) {
                var r = e.bottom >= 0 && e.top + e.height * SHOW_PART_RATE < this.windowHeight, t = e.right >= 0 && e.left + e.width * SHOW_PART_RATE < this.windowWidth;
                return r && t;
            }
        }
    } ]), e;
}();