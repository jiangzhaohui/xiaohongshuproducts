function _classCallCheck(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
}

function getTimeStampByDay() {
    return 1e3 * (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0) * 60 * 60 * 24;
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var _createClass = function() {
    function e(e, t) {
        for (var n = 0; n < t.length; n++) {
            var r = t[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), 
            Object.defineProperty(e, r.key, r);
        }
    }
    return function(t, n, r) {
        return n && e(t.prototype, n), r && e(t, r), t;
    };
}(), Datetime = function() {
    function e() {
        _classCallCheck(this, e);
    }
    return _createClass(e, [ {
        key: "getFormatObj",
        value: function(e) {
            var t = new Date(e), n = {
                year: t.getFullYear(),
                month: t.getMonth() + 1,
                day: t.getDate(),
                hour: t.getHours(),
                minute: t.getMinutes(),
                seconds: t.getSeconds()
            }, r = {};
            for (var a in n) Object.prototype.hasOwnProperty.call(n, a) && (r[a] = this.formatNumber(n[a]));
            return r;
        }
    }, {
        key: "formatNumber",
        value: function(e) {
            return e < 10 ? "0" + e : e.toString();
        }
    }, {
        key: "getTimeStampByDay",
        value: function() {
            return 1e3 * (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0) * 60 * 60 * 24;
        }
    } ]), e;
}();

exports.default = new Datetime(), exports.getTimeStampByDay = getTimeStampByDay;