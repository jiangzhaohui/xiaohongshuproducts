function _interopRequireDefault(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

function requestToken() {
    var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0], r = _api2.default.getStorageSync(_enum.STORAGE_KEY.API_TOKEN);
    return e || !r ? (req || (req = _getToken().then(function(e) {
        return req = void 0, Promise.resolve(e);
    })), req) : Promise.resolve(r);
}

function refreshTokenRegister() {
    task && clearInterval(task), task = setInterval(function() {
        requestToken(!0);
    }, 6e5);
}

Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.encryptToken = void 0, exports.requestToken = requestToken, exports.refreshTokenRegister = refreshTokenRegister;

var _enum = require("./enum.js"), _api = require("./api.js"), _api2 = _interopRequireDefault(_api), _md = require("./../libs/md5.js"), _md2 = _interopRequireDefault(_md), _urlParse = require("./../npm/url-parse/index.js"), _urlParse2 = _interopRequireDefault(_urlParse), _Base = require("./../npm/Base64/base64.js"), _Base2 = _interopRequireDefault(_Base), decodeJWT = function() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", r = e.split("."), t = "", a = "", n = "";
    try {
        t = JSON.parse(_Base2.default.atob(r[0])), a = JSON.parse(_Base2.default.atob(r[1])), 
        n = r[2];
    } catch (e) {}
    return {
        header: t,
        payload: a,
        sign: n
    };
}, encryptToken = exports.encryptToken = function() {
    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "", r = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, t = decodeJWT(e);
    if ("md4" === t.header.hash) {
        var a = (0, _urlParse2.default)(r.url);
        return (0, _md2.default)(a.pathname + a.query + t.payload.sid + t.header.hash);
    }
}, _getToken = function() {
    return new Promise(function(e) {
        _api2.default.request({
            url: "https://www.xiaohongshu.com/sapi/token?v=v3",
            method: "get",
            data: {},
            success: function(r) {
                var t = "";
                try {
                    t = r.data.data.token;
                } catch (e) {
                    console.error(e);
                }
                t && _api2.default.setStorage({
                    key: _enum.STORAGE_KEY.API_TOKEN,
                    data: t,
                    expire: 1
                }), e(t);
            },
            fail: function() {
                e({});
            }
        });
    });
}, req = void 0, task = void 0;