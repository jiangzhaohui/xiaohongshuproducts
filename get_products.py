import  requests
import json
import time 
import random
import re 
import ast 
from ast import literal_eval
from lxml import etree 
from fake_useragent import UserAgent
from multiprocessing.dummy import Pool as ThreadPool
import gevent 
from gevent import monkey 
gevent.monkey.patch_all()
from gevent.pool import Pool
from get_stores import *
from configs import db

ua = UserAgent()	
headers = {'User-Agent': ua.random}
redis = db.ReidsClient()


def get_categroy_list(seller_name):
	categories_id = []
	category_url = 'https://www.xiaohongshu.com/api/store/ps/filters/v2?keyword=' + seller_name
	try:
		rsp = requests.get(category_url, headers=headers, proxies=redis.random(), timeout=15)
	except:
		return get_categroy_list(seller_name)
	else:
		raw_categories = (json.loads(rsp.text)).get('data').get('filter_groups')[1].get('filter_tags')
		for items in raw_categories:	
			categories_id.append(items.get('title'))
		return categories_id


def parse_store_products(seller):
	print('正在爬取店铺：', seller.get('seller_name'))
	url = 'https://www.xiaohongshu.com/api/store/ps/products/v1?keyword=' + seller.get('seller_name') + '&filters=%5B%5D&page=1&size=1000' 
	i = 1
	while i < 10:
		try:
			rsp = requests.get(url, headers=headers, proxies=redis.random(), timeout=15)
			if rsp.status_code == 200:
				data = json.loads(rsp.text)
				if data.get('success') and len(data.get('data').get('items')):
					break
		except:
			print('第' + str(i) +'请求:', seller.get('seller_name'))
			i += 1

	if  data.get('data').get('sellers'):
		seller_id = data.get('data').get('sellers')[0].get('id')
		seller_name = data.get('data').get('sellers')[0].get('title')
		products_num = data.get('data').get('sellers')[0].get('total_items')
		print(seller_name, '店铺总商品数：', products_num)
		if products_num <= 1000:
			items = data.get('data').get('items')
			datas = []
			for item in items:
				data = parse_details(item)
				datas.append(data)
			db.save_products_data(seller_id, seller_name, datas)
		else:
			categories = get_categroy_list(seller_name)
			print('店铺商品类别：', categories)
			for category in categories:
				url = 'https://www.xiaohongshu.com/api/store/ps/products/v1?keyword=' + seller_name + category + '&filters=%5B%5D&page=1&size=1000'
				while True:
					try:
						rsp = requests.get(url, headers=headers, proxies=redis.random(), timeout=15)
						if rsp.status_code == 200:
							break
					except:
						pass
				if len(rsp.text) > 200:
					text = rsp.text
					print('正在爬取"%s",类别id：%s； 状态码：%s, 返回数据大小：%s' % (seller_name, category, rsp.status_code, len(rsp.text)))
					if len(text) <= 100:
						pass
					elif rsp.status_code == 200 and len(text) > 200:
						items = json.loads(text)
						if items.get('success') and items.get('data'):
							datas = []
							for item in items.get('data').get('items'):
								data = parse_details(item)
								datas.append(data)
							print(len(datas))
							db.save_products_data(seller_id, seller_name, datas)
				else:
					break


def parse_details(item):
	data = {}
	data['id'] = item.get('id')
	data['seller_id'] = item.get('seller_id')
	data['title'] = item.get('title') 
	data['desc'] = item.get('desc') 
	data['price'] = item.get('price')
	data['discount_price'] = item.get('discount_price')
	data['stock_status'] = item.get('stock_status') 
	try:
		data['link'] = item.get('link')[item['link'].index('www'):]
	except:
		pass
	data['promotion_text'] = item.get('promotion_text')
	data['vendor_icon'] = item.get('vendor_icon')
	data['vendor_name'] = item.get('vendor_name') 
	data['vnedor_link'] = item.get('vnedor_link') 
	data['buyable'] = item.get('buyable')
	data['new_arriving'] = item.get('new_arriving') 
	data['feature'] = item.get('feature')
	data['time'] = item.get('time')
	data['tax_included'] = item.get('tax_included')
	try:
		data['origin_price'] = item.get('item_price').get(1).get('price')
	except:
		pass
	try:
		data['sale_price'] = item.get('item_price').get(0).get('price')
	except:
		pass
	data['tags'] = item.get('tags')
	data['height'] = item.get('height')
	data['width'] = item.get('width') 
	data['stock_shortage'] = item.get('stock_shortage')


if __name__ == '__main__':
	# 获取所有店铺
	with open('categories.txt', 'r', encoding='utf-8') as f:
		raw_list = f.read()

	products_list = raw_list.split(',')
	pool = Pool(100)
	pool.map(start_requests, [keyword for keyword in products_list[:]])
	pool.kill()
	pool.join()

	# 获取店铺商品
	items = db.read_stores_data()
	pool = Pool(100)
	pool.map(parse_store_products, [item for item in items])
	pool.kill()
	pool.join()
